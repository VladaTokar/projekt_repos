﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Chat.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class StronaDomowa : ContentPage
	{
		public StronaDomowa()
		{
			SetValue(NavigationPage.HasNavigationBarProperty, false);
			InitializeComponent();

			image.Source = ImageSource.FromUri(new Uri("https://scontent-waw1-1.xx.fbcdn.net/v/t1.0-9/102588726_295061048184273_5480860491444125696_n.jpg?_nc_cat=103&_nc_sid=09cbfe&_nc_ohc=030KfAm7qoQAX8r383C&_nc_ht=scontent-waw1-1.xx&oh=17feb6b080fe4fe623d6861e375bee72&oe=5EFAED1C"));
		}
		async void Btn_wyloguj(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new Logowanie());
		}

	}
}